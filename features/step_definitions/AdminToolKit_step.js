var data = require('../../TestResources/AdminToolKit_data.js');
var Objects = require(__dirname + '/../../repository/AdminToolKit_locators.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var fs = require('fs');
var AdminToolKit , browser;
function initializePageObjects(browser, callback) {
    var HPage = browser.page.AdminToolKit_locators();
    AdminToolKit = HPage.section.AdminToolKit;
    callback();
}

module.exports = function() {

    this.Given(/^User opens AF URL in DEV region$/, function () {
        browser = this;
        var URL;
       initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.AFDEVURL);
            browser.timeoutsImplicitWait(60000);

        });

    });

    // module.exports = function() {
    //     this.Given(/^User opens AF URL in DEV region$/, function () {
    //         var URL;
    //         browser = this;
    //         URL = data.AFDEVURL;
    //             initializePageObjects(browser, function () {
    //                 browser.maximizeWindow()
    //                     .deleteCookies()
    //                     .url(URL);
    //                 browser.timeoutsImplicitWait(30000);
    //
    //             });
    //
    //     });


        this.When(/^User clicks on Administration Link$/, function () {
        AdminToolKit.waitForElementVisible('@lnk_Administration', data.wait)
            .click('@lnk_Administration')
    });
    this.When(/^User Clicks on Manage Adminsitrators$/, function () {
        AdminToolKit.waitForElementVisible('@lnk_Manage_Administrator', data.wait)
            .click('@lnk_Manage_Administrator')
    });
    this.When(/^User Search for the user and clicks on Find Now$/, function () {
        AdminToolKit.waitForElementVisible('@lnk_text_box', data.wait)
            .setValue('@lnk_text_box', data.Name1);
        AdminToolKit.waitForElementVisible('@lnk_find_now', data.wait)
            .click('@lnk_find_now');
    });
    this.When(/^User clicks on the "([^"]*)" username$/, function (locator) {
        AdminToolKit.waitForElementVisible('@'+locator+'', data.wait)
            .click('@'+locator+'')
    });

    this.When(/^User checks for the Groups "QA_Analyst_TEST_CLIENTS" and "AppDev_G2_Analyst_All_except_MMC"$/, function () {
        AdminToolKit.waitForElementVisible('@AppDev_G2_Analyst_All_except_MMC', data.wait)
        AdminToolKit.waitForElementVisible('@QA_Analyst_TEST_CLIENTS', data.wait)
    });
    this.When(/^If the User is assigned to the above groups then remove the groups$/, function () {
        AdminToolKit.waitForElementVisible('@lnk_Edit_Group', data.wait)
            .click('@lnk_Edit_Group')
        AdminToolKit.waitForElementVisible('@lnk_AppDev_G2_Analyst_All_except_MMC_after_edit', data.wait)
            .click('@lnk_AppDev_G2_Analyst_All_except_MMC_after_edit')
        AdminToolKit.waitForElementVisible('@lnk_QA_Analyst_TEST_CLIENTS_after_edit', data.wait)
            .click('@lnk_QA_Analyst_TEST_CLIENTS_after_edit')
        AdminToolKit.waitForElementVisible('@lnk_Save_Group', data.wait)
            .click('@lnk_Save_Group')
        AdminToolKit.waitForElementVisible('@lnk_AF_confirmation_msg', data.wait)

    });
    this.Given(/^User opens Admin ToolKit URL$/, function () {
        browser = this;
        var AFURL;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.AdminTKURL);
            browser.timeoutsImplicitWait(30000);

        });

    });
    // this.When(/^User finds Error message with No permission and No Clients"$/, function () {
    //
    //
    // });
    this.When(/^User finds Error message with No permission and No Clients$/, function () {
        AdminToolKit.waitForElementVisible('@lnk_AdminToolKit_Error', data.wait)
        AdminToolKit.waitForElementVisible('@lnk_AdminToolKit_no_per', data.wait)
        AdminToolKit.waitForElementVisible('@lnk_AdminToolKit_no_client', data.wait)
    });
    // this.When(/^User should see confirmation message$/, function () {
    //     AdminToolKit.waitForElementPresent('@lnk_AF_confirmation_msg', data.wait);
    //     AdminToolKit.getText('@lnk_AF_confirmation_msg', function(actual) {
    //     console.log(actual);
    //     });
    this.Given(/^Open the HTML file$/, function () {
        browser = this;
        var URL;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.AdminToolKitHTML);
            browser.timeoutsImplicitWait(60000);

        });

    });
    this.When(/^Check Admin TooKit is vulnerable or not$/, function () {
        AdminToolKit.waitForElementVisible('@lnk_Expected_Text', data.wait)
            });

    this.Given(/^User opens the browser and launch the Admin ToolKit URl$/, function () {
        browser = this;
        var URL;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.AdminTKCITEST1);
            browser.timeoutsImplicitWait(60000);

        });

    });
    this.When(/^Check if the protocol is changed from HTTP to HTTPS$/, function () {
        browser.assert.urlEquals("https://usba.mercer.com/webconfig/");
        // AdminToolKit.waitForElementVisible('@lnk_Expected_Text', data.wait)
    });

    this.Given(/^Verify that the URL should open only using HTTPS protocol$/, function () {
        browser = this;
        var URL;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.AdminTKCITEST2);
            browser.timeoutsImplicitWait(60000);
            AdminToolKit.waitForElementVisible('@title_page', data.wait)
        });

    });
    this.Given(/^User assigns the same group to the user$/, function () {
        browser = this;
        var URL;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(data.AFDEVURL);
            browser.timeoutsImplicitWait(60000);
            AdminToolKit.waitForElementVisible('@lnk_Administration', data.wait)
                .click('@lnk_Administration')
            AdminToolKit.waitForElementVisible('@lnk_Manage_Administrator', data.wait)
                .click('@lnk_Manage_Administrator')
            AdminToolKit.waitForElementVisible('@lnk_text_box', data.wait)
                .setValue('@lnk_text_box', data.Name1);
            AdminToolKit.waitForElementVisible('@lnk_find_now', data.wait)
                .click('@lnk_find_now');
            AdminToolKit.waitForElementVisible('@User_Name', data.wait)
                .click('@User_Name')
            AdminToolKit.waitForElementVisible('@lnk_Edit_Group', data.wait)
                .click('@lnk_Edit_Group')
            AdminToolKit.waitForElementVisible('@lnk_AppDev_G2_Analyst_All_except_MMC_after_edit', data.wait)
                .click('@lnk_AppDev_G2_Analyst_All_except_MMC_after_edit')
            AdminToolKit.waitForElementVisible('@lnk_QA_Analyst_TEST_CLIENTS_after_edit', data.wait)
                .click('@lnk_QA_Analyst_TEST_CLIENTS_after_edit')
            AdminToolKit.waitForElementVisible('@lnk_Save_Group', data.wait)
                .click('@lnk_Save_Group')
            AdminToolKit.waitForElementVisible('@lnk_AF_confirmation_msg', data.wait)
            AdminToolKit.waitForElementVisible('@AppDev_G2_Analyst_All_except_MMC', data.wait)
            AdminToolKit.waitForElementVisible('@QA_Analyst_TEST_CLIENTS', data.wait)
        });
});
    this.When(/^User must not use the application for the specified time limit$/, function () {
        AdminToolKit.waitForElementVisible('@client_list', data.logoutwait)
    });
    this.When(/^The Application has automatically logged you out$/, function () {
        AdminToolKit.waitForElementNotPresent('@client_list', data.logoutwait)
    });
    this.When(/^Admin ToolKit title should be displayed$/, function () {
        AdminToolKit.waitForElementVisible('@benefit_hub_logo', data.wait)
    });
    this.When(/^Go to Data File page and upload unexpected file$/, function () {
        AdminToolKit.waitForElementVisible('@QAACME', data.midwait)
            .click('@QAACME');
        AdminToolKit.waitForElementVisible('@element_to_wait', data.wait)
        AdminToolKit.setValue('@SearchBox', data.Text)
           .click('@SearchBox');
        AdminToolKit.waitForElementVisible('@Benefit_Commencement_Tile_Image', data.wait)
            .click('@Benefit_Commencement_Tile_Image');
        AdminToolKit.waitForElementVisible('@Add_Rule', data.wait)
            .click('@Add_Rule');
        AdminToolKit.waitForElementVisible('@Update_link', data.wait)
            .click('@Update_link');
        AdminToolKit.waitForElementVisible('@Upload_New', data.wait)
            .click('@Upload_New');
        AdminToolKit.setValue('@keywords_textbox', data.Text)
            .click('@keywords_textbox');
        AdminToolKit.setValue('@Description_textbox', data.Text)
            .click('@Description_textbox');
        // AdminToolKit.waitForElementVisible('@choose_file', data.wait)
        //    .click('@choose_file');
        browser.pause(5000)
        browser.useCss().setValue('#upload-media-library-file',require('path').resolve('C:\\Users\\Shashank-Kumar\\Desktop\\abcde.xlsx'));
       // action.UploadFile('C:\\Users\\Shashank-Kumar\\Desktop\\abcde.xlsx',function
        AdminToolKit.waitForElementVisible('@Save_and_select', data.wait)
            .click('@Save_and_select');
    });
    this.Then(/^Application should not allow the upload of unexpected file types and Throws error$/, function () {
        AdminToolKit.waitForElementVisible('@final_Error_message', data.wait)
    });

    this.When(/^User selects "([^"]*)" client$/, function (locator) {
        AdminToolKit.waitForElementVisible('@'+locator+'', data.midwait)
            .click('@'+locator+'');
    });
    this.When(/^If the User  is not assigned to the above groups then reassign the groups$/, function () {
        AdminToolKit.waitForElementVisible('@AppDev_G2_Analyst_All_except_MMC', data.wait)
        AdminToolKit.waitForElementVisible('@QA_Analyst_TEST_CLIENTS', data.wait)

    });
    this.When(/^User is reassigned the groups$/, function () {
        AdminToolKit.waitForElementVisible('@lnk_Edit_Group', data.wait)
            .click('@lnk_Edit_Group')
        AdminToolKit.waitForElementVisible('@lnk_AppDev_G2_Analyst_All_except_MMC_after_edit', data.wait)
            .click('@lnk_AppDev_G2_Analyst_All_except_MMC_after_edit')
        AdminToolKit.waitForElementVisible('@lnk_QA_Analyst_TEST_CLIENTS_after_edit', data.wait)
            .click('@lnk_QA_Analyst_TEST_CLIENTS_after_edit')
        AdminToolKit.waitForElementVisible('@lnk_Save_Group', data.wait)
            .click('@lnk_Save_Group')
        AdminToolKit.waitForElementVisible('@lnk_AF_confirmation_msg', data.wait)

    });

}



