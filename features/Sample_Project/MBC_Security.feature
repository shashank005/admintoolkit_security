Feature: Security scenarios for MBC Admin Portal

  @SecSc1
  Scenario: To verify the Admin toolkit authorization is working as per the roles selected in AF
    Given User opens AF URL in DEV region
    And User clicks on Administration Link
    And  User Clicks on Manage Adminsitrators
    And User Search for the user and clicks on Find Now
    And User clicks on the "User_Name" username
    And User checks for the Groups "QA_Analyst_TEST_CLIENTS" and "AppDev_G2_Analyst_All_except_MMC"
    When If the User is assigned to the above groups then remove the groups
    And User opens Admin ToolKit URL
    And User finds Error message with No permission and No Clients
   And User assigns the same group to the user

  @SecSc2
  Scenario: To verify whether Admin TooKit is vulnerable to click jacking
    Given  Open the HTML file
    Then   Check Admin TooKit is vulnerable or not

  @SecSc3
  Scenario: To verify HTTP STRICT TRANSPORT SECURITY
    Given   User opens the browser and launch the Admin ToolKit URl
    When    Check if the protocol is changed from HTTP to HTTPS
    Then    Verify that the URL should open only using HTTPS protocol

  @SecSc4
    #Not applicable
  Scenario: To verify session timeout in MBC Admin Toolkit application
    Given User opens Admin ToolKit URL
   And User must not use the application for the specified time limit
   Then The Application has automatically logged you out

  @SecSc5
  Scenario: To verify that the application does not allow the upload of unexpected file types
    Given User opens AF URL in DEV region
    And User clicks on Administration Link
    And  User Clicks on Manage Adminsitrators
    And User Search for the user and clicks on Find Now
    And User clicks on the "User_Name" username
    When If the User  is not assigned to the above groups then reassign the groups
    And  User opens Admin ToolKit URL
    And  Admin ToolKit title should be displayed
    And  User selects "QAACME" client
    And  Go to Data File page and upload unexpected file
    Then Application should not allow the upload of unexpected file types and Throws error

  @AssignGroup
  Scenario: To reassign the groups to the user
    Given User opens AF URL in DEV region
    And User clicks on Administration Link
    And  User Clicks on Manage Adminsitrators
    And User Search for the user and clicks on Find Now
    And User clicks on the "User_Name" username
    When User is reassigned the groups


@Dummy

  Scenario: This is a dummy Scenario
    Given User opens AF URL in DEV region
    And User clicks on Administration Link
    And  User Clicks on Manage Adminsitrators
    And User Search for the user and clicks on Find Now
    And User clicks on the "User_Name" username
    When User is reassigned the groups

