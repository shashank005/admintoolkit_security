/**
 * Created by Shashank-Kumar on 1/4/2018.
 */
/**
 * Created by Shashank-Kumar on 11/21/2017.
 */
module.exports = {
    sections: {
        AdminToolKit: {
            selector: 'body',
            elements: {
                lnk_Administration: {locateStrategy: 'xpath', selector: "(//td//a[contains(.,'Administration')])[1]"},
                lnk_Manage_Administrator: {
                    locateStrategy: 'xpath', selector: "//a[contains(text(),'Manage Administrators')]"},
                lnk_text_box: {locateStrategy: 'xpath', selector: "(//td//input[@type='text'])[2]"},
                lnk_find_now: {locateStrategy: 'xpath', selector: "//input[@value='Find Now']"},
                User_Name: {locateStrategy: 'xpath', selector: "//a[contains(.,'Kumar, Shashank')]"},
                User_Name1: {locateStrategy: 'xpath', selector: "//a[contains(.,'Pant, Bhaskar')]"},
                lnk_QA_Analyst_TEST_CLIENTS: {locateStrategy: 'xpath', selector: "(//li[contains(text(),'QA-Analyst-TEST CLIENTS')])[1]"},
                lnk_QA_Analyst_TEST_CLIENTS1: {locateStrategy: 'xpath', selector: "//label[contains(text(),'QA-Analyst-TEST CLIENTS')]"},
                lnk_AppDev_G2_Analyst_All_except_MMC: {locateStrategy: 'xpath', selector: "//td//label[contains(.,'AppDev G2-Analyst-All except MMC')]"},
                lnk_AppDev_G2_Analyst_All_except_MMC1: {locateStrategy: 'xpath', selector: "//label[contains(text(),'AppDev G2-Analyst-All except MMC')]"},
                lnk_Edit_Group: {locateStrategy: 'xpath',selector: "(//div//a[contains(.,'Edit')])[2]"},
                lnk_Save_Group: {locateStrategy: 'xpath',selector: "(//input[@class='geo-sa-button'])[1]"},
                lnk_AF_confirmation_msg: {locateStrategy: 'xpath',selector: "//span[@title='Confirmation']"},
                lnk_AdminToolKit_Error: {locateStrategy: 'xpath',selector: "//div/h4[contains(.,'Error')]"},
                lnk_AdminToolKit_no_per: {locateStrategy: 'xpath',selector: "//div/span[contains(.,'No permissions')]"},
                lnk_AdminToolKit_no_client: {locateStrategy: 'xpath',selector: "//td/p[contains(.,'No clients available')]"},
                lnk_QA_Analyst_TEST_CLIENTS_after_edit: {locateStrategy: 'xpath', selector: "//label[contains(.,'QA-Analyst-TEST CLIENTS')]"},
                lnk_AppDev_G2_Analyst_All_except_MMC_after_edit: {locateStrategy: 'xpath', selector: "//td//label[contains(.,'AppDev G2-Analyst-All except MMC')]"},
                lnk_Expected_Text: {locateStrategy: 'xpath',selector: "//p[contains(.,'This website is vulnerable to Clickjacking')]"},
                title_page: {locateStrategy: 'xpath',selector: "//a[contains(.,'BENEFITS HUB')]"},
                client_list: {locateStrategy: 'xpath',selector: "//div/h1[contains(.,'Client List')]"},
                benefit_hub_logo: {locateStrategy: 'xpath',selector: "//h1[@class='mul-logo']"},
                QAACME: {locateStrategy: 'xpath',selector: "//a[contains(.,'QAACME')]"},
                SearchBox: {locateStrategy: 'xpath',selector: "//input[@id='eval-points-filter']"},
                Benefit_Commencement_Tile_Image: {locateStrategy: 'xpath',selector: " //td/a[contains(.,'Benefit Commencement Tile Image')]"},
                Add_Rule: {locateStrategy: 'xpath',selector: "//a[contains(.,'Add Rule')]"},
                Rule_Name: {locateStrategy: 'xpath',selector: "//input[@title ='Enter a rule name.']"},
                Rule_Description: {locateStrategy: 'xpath',selector: "//input[@title ='Enter a rule description.']"},
                Update_link:  {locateStrategy: 'xpath',selector: "//a[contains(.,'{Update}')]"},
                Upload_New: {locateStrategy: 'xpath',selector: "//a[@class='mul-image-icon mul-image-icon-upload ui-tabs-anchor']"},
                keywords_textbox: {locateStrategy: 'xpath',selector: "//input[@name='keywords']"},
                Description_textbox: {locateStrategy: 'xpath',selector: "//input[@name='description']"},
                Save_and_select: {locateStrategy: 'xpath',selector: "//button[@title='Save and Select']"},
                choose_file: {locateStrategy: 'xpath',selector: "//input[@id='upload-media-library-file']"},
                element_to_wait: {locateStrategy: 'xpath',selector: "(//td[contains(.,'HB Data Service')])[1]"},
                final_Error_message: {locateStrategy: 'xpath',selector: "//p[contains(text(),'You must select a valid file type to upload')]/../..//h6[contains(text(),'Error')]"},
                AppDev_G2_Analyst_All_except_MMC: {locateStrategy: 'xpath',selector: "//li//ul//li[contains(.,'AppDev G2-Analyst-All except MMC')]"},
                QA_Analyst_TEST_CLIENTS: {locateStrategy: 'xpath',selector: "//li//ul//li[contains(.,'QA-Analyst-TEST CLIENTS')]"},
            }
        }
    }
}

